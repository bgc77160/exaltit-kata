package com.exalt.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.IntBinaryOperator;
import java.util.regex.Pattern;

public class StringCalculatorKata {
    // Why this method isn't static ?
    int add(String numbers) {
        if (numbers == null || numbers.isBlank()) {
            return 0;
        }
        if (!numbers.startsWith("//")) {
            return add(numbers, ",");
        }
        var tokens = numbers.split("\\n", 2);
        if (tokens.length != 2) {
            throw new IllegalArgumentException("Missing number to parse");
        }
        return add(tokens[1], Pattern.quote(tokens[0].substring(2)));
    }
    
    private static int add(String numbers, String separator) {
        var tokens = numbers.split(separator + "|\\n", -1); // -1 force the method to keep trailing empty string
        var illegalNumbers = new ArrayList<Integer>();
        int sum = Arrays.stream(tokens)
                        .mapToInt(StringCalculatorKata::parseInt)
                        .reduce(0, reducerFactory(illegalNumbers));
        if (!illegalNumbers.isEmpty()) {
            throw new NegativeNotAllowedException(illegalNumbers);
        }
        return sum;
    }
    
    private static IntBinaryOperator reducerFactory(List<Integer> illegalNumbers) {
        return (left, right) -> {
            if (right < 0) {
                illegalNumbers.add(right);
            } else {
                left += right;
            }
            return left;
        };
    }
    
    private static int parseInt(String input) {
        if (input == null || input.isBlank()) {
            throw new IllegalArgumentException("Cannot have empty value between separators");
        }
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException("Cannot parse the value " + input);
        }
    }
}

