package com.exalt.company;

import java.util.List;

public class NegativeNotAllowedException extends RuntimeException {
    public NegativeNotAllowedException(List<Integer> numbers) {
        super("negative not allowed " + numbers);
    }
}
