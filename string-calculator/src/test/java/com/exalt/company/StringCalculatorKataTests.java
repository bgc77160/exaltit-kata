package com.exalt.company;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class StringCalculatorKataTests {
    // Immutable object.
    private static final StringCalculatorKata calculator = new StringCalculatorKata();
    
    private static Stream<Arguments> firstStepSuccessTests() {
        return Stream.of(
                arguments(null, 0),
                arguments("", 0),
                arguments("1", 1),
                arguments("1,2", 3)
        );
    }
    
    @ParameterizedTest
    @MethodSource
    void firstStepSuccessTests(String numbers, int expected) {
        assertEquals(calculator.add(numbers), expected);
    }
    
    private static Stream<Arguments> firstStepExceptionTests() {
        return Stream.of(
                arguments("a", IllegalArgumentException.class),
                arguments("1a", IllegalArgumentException.class),
                arguments("a1", IllegalArgumentException.class),
                arguments("1,a", IllegalArgumentException.class)
        );
    }
    
    @ParameterizedTest
    @MethodSource
    void firstStepExceptionTests(String numbers, Class<? extends Throwable> exception) {
        assertThrows(exception, () -> calculator.add(numbers));
    }
    
    private static Stream<Arguments> secondStepSuccessTests() {
        return Stream.of(
                arguments("1,2,3", 6),
                arguments("1,2,3,4,5,6,7,8,9,10", 55)
        );
    }
    
    @ParameterizedTest
    @MethodSource
    void secondStepSuccessTests(String numbers, int expected) {
        assertEquals(calculator.add(numbers), expected);
    }
    
    private static Stream<Arguments> thirdStepSuccessTests() {
        return Stream.of(
                arguments("1\n2,3", 6),
                arguments("1\n2\n3", 6)
        );
    }
    
    @ParameterizedTest
    @MethodSource
    void thirdStepSuccessTests(String numbers, int expected) {
        assertEquals(calculator.add(numbers), expected);
    }
    
    @ParameterizedTest
    @ValueSource(strings = {
            "1,\n",
            "1,",
            "1\n",
            "1\n\n\n\n",
            ",,,,,"
    })
    void thirdStepExceptionTests(String numbers) {
        assertThrows(IllegalArgumentException.class, () -> calculator.add(numbers));
    }
    
    private static Stream<Arguments> forthStepSuccessTests() {
        return Stream.of(
                arguments("//;\n1;2", 3),
                arguments("//x\n1x2\n3", 6),
                arguments("//separator\n1separator2\n3", 6),
                arguments("//.*\n1.*2\n3", 6),
                arguments("//;\n", 0)
        );
    }
    
    @ParameterizedTest
    @MethodSource
    void forthStepSuccessTests(String numbers, int expected) {
        assertEquals(calculator.add(numbers), expected);
    }
    
    @ParameterizedTest
    @ValueSource(strings = {
            "//;\n1,2",
            "//;"
    })
    void forthStepExceptionTests(String numbers) {
        assertThrows(IllegalArgumentException.class, () -> calculator.add(numbers));
    }
    
    private static Stream<Arguments> fifthStepExceptionTests() {
        return Stream.of(
                arguments("-1", "negative not allowed [-1]"),
                arguments("-1,2", "negative not allowed [-1]"),
                arguments("2,-1", "negative not allowed [-1]"),
                arguments("-1,-2", "negative not allowed [-1, -2]"),
                arguments("-1\n-2", "negative not allowed [-1, -2]")
        );
    }
    
    @ParameterizedTest
    @MethodSource
    void fifthStepExceptionTests(String numbers, String message) {
        assertThrowsExactly(NegativeNotAllowedException.class, () -> calculator.add(numbers), message);
    }
}
