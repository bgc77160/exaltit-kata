# String Calculator Kata



## Requirements
- [x] Java 8+ => Java 17
- [x] Maven 3
- [x] Junit 5 => Junit 5.9.0
- [x] Use StringCalculatorKata and don't change project structure

### Step 1
Goto StringCalculatorKata and put your first implementation of:

```
 int add(String numbers)
```
The method can take up to two numbers, separated by commas, and will return their sum.

For example “” or “1” or “1,2” as inputs.

For an empty string it will return 0.

### Step 2

Allow the Add method to handle an unknown amount of numbers.

### Step 3

Allow the "add" method to handle new lines between numbers (instead of commas):

- The following input is ok: “1\n2,3” (will equal 6)
- The following input is NOT ok: “1,\n” (not need to prove it - just clarifying)

### Step 4
Support different delimiters:

- To change a delimiter, the beginning of the string will contain a separate line that looks like this: “//[delimiter]\n[numbers…]” for example “//;\n1;2” should return three where the default delimiter is ‘;’.
- The first line is optional. All existing scenarios should still be supported.

### Step 5
Calling "add" with a negative number will throw an exception “negatives not allowed” - and the negative that was passed.
If there are multiple negatives, show all of them in the exception message.

### Step 6
Send us your public repository with StringCalculatorKata implementation so that we can give you feedback

## Credits
Inspired by Roy Osherove [https://osherove.com/]



# Implementation

This part is here to give you information about my implementation of the string calculator.

## Step 1

- Handle null and empty string will give 0 as result.
- Handle not-number string will throw an IllegalArgumentException (IAE)
- Handle string with more than 2 arguments will throw a custom exception
- Handle empty value within string and replace them with 0 (i.e. "1," is equals to "1,0" or "1")

## Step 2

- Remove custom exception thrown when more than 2 arguments are parsed.

## Step 3

- Handling empty value as en error (i.e. "1,\n" shouldn't be supported so "1," will not be supported anymore)
  - This changement broke the backward compatibility but must be done to respect step 3 rules.

## Step 4

- No precisions about if the delimiter should be a character or a string, so I choose to allow a string as delimiter.
- The input can still have multiple lines
- The delimiter can't be a regex

## Step 5

Nothing special about implementation